FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/app

COPY pom.xml /usr/src/app
COPY ./src /usr/src/app/src
RUN mvn package

ENV PORT 8080
EXPOSE $PORT
CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
