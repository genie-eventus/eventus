import axios from 'axios'

const EVENEMENTS_REST_API_URL = 'http://localhost:8080/api/v1/evenement';

class EvenementService {

    getEvenements(){
        return axios.get(EVENEMENTS_REST_API_URL);
    }
    createEvenement(evenement){
        return axios.post(EVENEMENTS_REST_API_URL, evenement);
    }

    getEvenementById(evenementId){
        return axios.get(EVENEMENTS_REST_API_URL + '/' + evenementId);
    }

    updateEvenement(evenement, evenementId){
        return axios.put(EVENEMENTS_REST_API_URL + '/' + evenementId, evenement);
    }

    deleteEvenement(evenementId){
        return axios.delete(EVENEMENTS_REST_API_URL + '/' + evenementId);
    }
}

export default new EvenementService();