import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Footer from './components/footer';
import Navigationbar from "./components/navbar";
import Nomatch from "./components/nomatch"
import AjouterEvenement from "./components/AjouterEvenement";
import AfficherEvenement from "./components/AfficherEvenement";
import ViewEvenement from "./components/ViewEvenement";
import UpdateEvenement from "./components/UpdateEvenement";



function App() {
  return (

      <Router>
          <div>
              <Navigationbar />
              <Switch>
                  <Route
                      exact path = "/"
                      component = {AfficherEvenement}
                  />
                  <Route
                      path = "/ajouter-evenement"
                      component = {AjouterEvenement}
                  />
                  <Route
                      path = "/voir-evenement/:id"
                      component = {ViewEvenement}
                  />
                  <Route
                      path = "/modifier-evenement/:id"
                      component = {UpdateEvenement}
                  />
                  <Route
                      path = "/"
                      component= {Nomatch}
                  />
              </Switch>
              <Footer />
          </div>
      </Router>
  );
}

export default App;
