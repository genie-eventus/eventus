import React, { Component } from "react";
import Jumbotron from 'react-bootstrap/Jumbotron';
import Container from 'react-bootstrap/Container';
import './hero.css';

class Hero extends Component {
    render() {
        return (
            <div>
                <Jumbotron>
                    <Container id="herocontainer">
                        <div id="wrapper">
                            <img
                                src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fs-ec.bstatic.com%2Fimages%2Fhotel%2Fmax1024x768%2F929%2F92985956.jpg&f=1&nofb=1"
                                id="heroimage"
                                alt=""
                            />
                        </div>
                        <div className="hero-text">
                            <button id="hero-button">Voir la liste d'&eacute;v&eacute;nements</button>
                        </div>
                    </Container>
                </Jumbotron>
            </div>
        )
    }
}

export default Hero
