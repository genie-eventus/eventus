import React, { Component } from "react";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import './footer.css';
//import { faGitlab } from '@fortawesome/free-brands-svg-icons';
//import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class Footer extends Component {
    render() {
        return (
            <div>
                <Container fluid>
                    <Row className="footerrow">
                        <Col className="footercol">
                            <b>Nous Contacter</b><br/>
                            {/*<FontAwesomeIcon icon={faEnvelope} />*/}
                            <a href="mailto:genie-eventus@groupes.usherbrooke.ca" id="footerlink1">genie-eventus@groupes.usherbrooke.ca</a><br />
                        </Col>
                        <Col className="footercol">
                            <b>Ressources Additionnelles</b><br/>
                            {/*<FontAwesomeIcon icon={faGitlab} />*/}
                            <a href="https://gitlab.com/genie-eventus/eventus" id="footerlink2">Code Source</a><br />
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default Footer
