import React, { Component } from "react";
import './nomatch.css'

class Nomatch extends Component {
    render() {
        return (
            <div>
                <p>
                    Cette page n'existe pas
                </p>
                <a href="/">
                    <h1>Retourner à la page d'accueil</h1>
                </a>
            </div>
        )
    }
}

export default Nomatch
