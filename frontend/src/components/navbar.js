import React, { Component } from "react";
import Navbar from 'react-bootstrap/Navbar';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import './navbar.css';
import logo from './eventuslogo540p.png';

class Navigationbar extends Component {
    render() {
        return (
            <div>
                <Navbar fixed="top">
                    <Container fluid>
                        <Row id="navbarrow">
                            <Col>
                                <Navbar.Brand href="/">
                                    <img
                                        src={logo}
                                        alt=""
                                        id="logo"
                                    />
                                    EventUS
                                </Navbar.Brand>
                                <a href="/ajouter-evenement" id="bouttonnavbar">Cr&eacute;er un &eacute;v&eacute;nement</a>
                            </Col>
                            <Col>
                                <a href="/connect" id="connexion">Connexion</a>
                            </Col>
                        </Row>
                    </Container>
                </Navbar>
            </div>
        )
    }
}

export default Navigationbar
