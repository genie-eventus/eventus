import React from 'react';
import EvenementService from "../services/EvenementService";
import Jumbotron from 'react-bootstrap/Jumbotron';
import Container from 'react-bootstrap/Container';
import './hero.css';
import './liste.css';

class AfficherEvenement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            evenements:[]
        }
        this.addEvenement = this.addEvenement.bind(this);
        this.editEvenement = this.editEvenement.bind(this);
        this.viewEvenement = this.viewEvenement.bind(this);
        this.deleteEvenement = this.deleteEvenement.bind(this);
    }

    deleteEvenement(id){
        EvenementService.deleteEvenement(id).then( res => {
            this.setState({evenement: this.state.evenements.filter(employee => employee.id !== id)});
        });
        window.location.reload();
    }

    viewEvenement(id){
        this.props.history.push(`/voir-evenement/${id}`);
    }

    editEvenement(id){
        this.props.history.push(`/modifier-evenement/${id}`);
    }

    componentDidMount() {
        EvenementService.getEvenements().then((response) => {
            this.setState({evenements: response.data})
        });
    }

    addEvenement(){
        this.props.history.push('/ajouter-evenement');
    }

    render() {
        return (
            <div>
                <div>
                    <Jumbotron>
                        <Container id="herocontainer">
                            <div id="wrapper">
                                <img
                                    src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fs-ec.bstatic.com%2Fimages%2Fhotel%2Fmax1024x768%2F929%2F92985956.jpg&f=1&nofb=1"
                                    id="heroimage"
                                    alt=""
                                />
                            </div>
                            <div className="hero-text">
                                <button id="hero-button">Voir la liste d'&eacute;v&eacute;nements</button>
                            </div>
                        </Container>
                    </Jumbotron>
                </div>
                <h1>Événements</h1>
                <div>
                    <table className = "table table-striped">
                        <thead>
                            <tr>
                                <td> Nom</td>
                                <td> Lieu</td>
                                <td> Date</td>
                                <td> Heure de Début</td>
                                <td> Heure de Fin</td>
                                <td> Actions</td>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.evenements.map(
                                    (evenement) =>
                                    <tr key={evenement.id}>
                                        <td> {evenement.nom}</td>
                                        <td> {evenement.lieu}</td>
                                        <td> {evenement.date}</td>
                                        <td> {evenement.heureDebut}</td>
                                        <td> {evenement.heureFin}</td>
                                        <td>
                                            <button onClick={ () => this.viewEvenement(evenement.id)} className="btn btn-info">Voir</button>
                                            <button style={{marginLeft: "10px"}} onClick={ () => this.editEvenement(evenement.id)} className="btn btn-info">Modifier</button>
                                            <button style={{marginLeft: "10px"}} onClick={ () => this.deleteEvenement(evenement.id)} className="btn btn-danger">Supprimer</button>
                                        </td>
                                    </tr>
                                )
                            }

                        </tbody>
                    </table>
                </div>

            </div>
        )
    }
}

export default AfficherEvenement
