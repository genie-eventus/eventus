import React, { Component } from 'react'
import EvenementService from "../services/EvenementService";
import './ViewEvenement.css';

class ViewEvenement extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            evenement: {}
        }
    }

    componentDidMount(){
        EvenementService.getEvenementById(this.state.id).then( res => {
            this.setState({evenement: res.data});
        })
    }

    render() {
        return (
            <div id="view-container">
                <div className = "row">
                    <img
                        src={ this.state.evenement.imageURL }
                        alt = "image non disponible"
                    />
                </div>
                <div className = "row">
                    <h1> { this.state.evenement.nom }</h1>
                </div>
                <div className = "row">
                    <h3> Lieu: </h3>
                    <div> { this.state.evenement.lieu }</div>
                </div>
                <div className = "row">
                    <h3> Description: </h3>
                    <div> { this.state.evenement.description }</div>
                </div>
                <div className = "row">
                    <h3> Créateur de l'événement: </h3>
                    <div> { this.state.evenement.cip }</div>
                </div>
                <div className = "row">
                    <h3> Date: </h3>
                    <div> { this.state.evenement.date }</div>
                </div>
                <div className = "row">
                    <h3> Heure de début: </h3>
                    <div> { this.state.evenement.heureDebut }</div>
                </div>
                <div className = "row">
                    <h3> Heure de fin: </h3>
                    <div> { this.state.evenement.heureFin }</div>
                </div>
            </div>
        )
    }
}

export default ViewEvenement
