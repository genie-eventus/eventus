import React, { Component } from 'react'
import EvenementService from "../services/EvenementService";
import './AjouterEvenement.css';

class AjouterEvenement extends Component {
    constructor(props) {
        super(props)

        this.state = {
            nom: '',
            lieu: '',
            imageURL: '',
            description: '',
            cip: '',
            date: '',
            heureDebut: '',
            heureFin: '',
            statutId: ''
        }
        this.changeNomHandler = this.changeNomHandler.bind(this);
        this.changeLieuHandler = this.changeLieuHandler.bind(this);
        this.changeImageURLHandler = this.changeImageURLHandler.bind(this);
        this.changeDescriptionHandler = this.changeDescriptionHandler.bind(this);
        this.changeCipHandler = this.changeCipHandler.bind(this);
        this.changeDateHandler = this.changeDateHandler.bind(this);
        this.changeHeureDebutHandler = this.changeHeureDebutHandler.bind(this);
        this.changeHeureFinHandler = this.changeHeureFinHandler.bind(this);
        this.changeStatutIdHandler = this.changeStatutIdHandler.bind(this);
        this.saveEvenement = this.saveEvenement.bind(this);
    }

    saveEvenement = (e) => {
        e.preventDefault();
        let evenement = {nom: this.state.nom, lieu: this.state.lieu, imageURL: this.state.imageURL,
            description: this.state.description, cip: this.state.cip, date: this.state.date,
            heureDebut: this.state.heureDebut, heureFin: this.state.heureFin, statutId: this.state.statutId};
        console.log('evenement => ' + JSON.stringify(evenement));
        EvenementService.createEvenement(evenement).then(res =>{
            this.props.history.push('/');
        });
    }

    changeNomHandler= (event) => {
        this.setState({nom: event.target.value});
    }

    changeLieuHandler= (event) => {
        this.setState({lieu: event.target.value});
    }

    changeImageURLHandler= (event) => {
        this.setState({imageURL: event.target.value});
    }

    changeDescriptionHandler= (event) => {
        this.setState({description: event.target.value});
    }

    changeCipHandler= (event) => {
        this.setState({cip: event.target.value});
    }

    changeDateHandler= (event) => {
        this.setState({date: event.target.value});
    }

    changeHeureDebutHandler= (event) => {
        this.setState({heureDebut: event.target.value});
    }

    changeHeureFinHandler= (event) => {
        this.setState({heureFin: event.target.value});
    }

    changeStatutIdHandler= (event) => {
        this.setState({statutId: event.target.value});
    }

    cancel(){
        this.props.history.push('/');
    }

    render() {
        return (
            <div>
                <h2 className="form-header">Ajouter un Evenement</h2>
                            <div>
                                <form>
                                    <div className = "form-group">
                                        <label> Nom: </label>
                                        <input placeholder="Nom" name="nom" className="form-control"
                                               value={this.state.nom} onChange={this.changeNomHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> Lieu: </label>
                                        <input placeholder="Lieu" name="lieu" className="form-control"
                                               value={this.state.lieu} onChange={this.changeLieuHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> URL d'image: </label>
                                        <input placeholder="URL image" name="imageURL" className="form-control"
                                               value={this.state.imageURL} onChange={this.changeImageURLHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> Description: </label>
                                        <input placeholder="Description" name="description" className="form-control"
                                               value={this.state.description} onChange={this.changeDescriptionHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> Cip: </label>
                                        <input placeholder="Cip" name="cip" className="form-control"
                                               value={this.state.cip} onChange={this.changeCipHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> Date: </label>
                                        <input placeholder="Date" type="date" name="date" className="form-control"
                                               value={this.state.date} onChange={this.changeDateHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> Heure Debut: </label>
                                        <input placeholder="Heure Debut" type="time" name="heureDebut" className="form-control"
                                               value={this.state.heureDebut} onChange={this.changeHeureDebutHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> Heure Fin: </label>
                                        <input placeholder="Heure Fin" type="time" name="heureFin" className="form-control"
                                               value={this.state.heureFin} onChange={this.changeHeureFinHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label> StatutId: </label>
                                        <input placeholder="StatutId" name="statutId" className="form-control"
                                               value={this.state.statutId} onChange={this.changeStatutIdHandler}/>
                                    </div>
                                </form>
                                <div className="button-container">
                                    <button className="button-save" onClick={this.saveEvenement}>Sauvegarder</button>
                                    <button className="button-cancel" onClick={this.cancel.bind(this)}>Annuler</button>
                                </div>
                            </div>
            </div>
        )
    }
}

export default AjouterEvenement
