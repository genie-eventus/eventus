#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER admin_eventus WITH ENCRYPTED PASSWORD 'passwd' SUPERUSER;
    CREATE DATABASE eventus_dev;
    GRANT ALL PRIVILEGES ON DATABASE eventus_dev TO admin_eventus;
    CREATE DATABASE eventus_test;
    GRANT ALL PRIVILEGES ON DATABASE eventus_test TO admin_eventus;
EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "eventus_dev" -f "/docker-entrypoint-initdb.d/V0_EventusDatabase.sql"
