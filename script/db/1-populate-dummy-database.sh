#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "admin_eventus" --dbname "eventus_dev" <<-EOSQL
    insert into Faculte (Faculte_nom) Values ('Faculte de Genie');
    insert into Programme (Programme_nom,FaculteID) Values ('Genie Informatique',1);
    insert into Programme (Programme_nom,FaculteID) Values ('Genie Electrique',1);
    insert into Role (Role_nom) Values ('etudiant dev');
    insert into Role (Role_nom) Values ('etudiant');
    insert into Role (Role_nom) Values ('prof');
    insert into Usager (CIP,Prenom,RoleID,ProgrammeID) Values ('casj1001','Daniel',1,1);
    insert into Usager (CIP,Prenom,RoleID,ProgrammeID) Values ('mare1709','Matthieu',1,1);
    insert into Usager (CIP,Prenom,RoleID,ProgrammeID) Values ('daom2504','Eloise',1,1);
    insert into Usager (CIP,Prenom,RoleID,ProgrammeID) Values ('lafj2918','Justine',1,1);
    insert into Usager (CIP,Prenom,RoleID,ProgrammeID) Values ('laba2123','Antoine',1,1);
    insert into Statut (Statut_nom) Values ('open');
    insert into Evenement (EventID, Event_nom, Event_lieu, Event_image, Event_description, CIP, Event_date, Heure_debut, Heure_fin, StatutID) values (uuid_generate_v4(),'Integ','Faculté de Génie','image','description','casj1001', '29-JUN-20', '11:03:20', '12:03:20',1);
    insert into Evenement (EventID, Event_nom, Event_lieu, Event_image, Event_description, CIP, Event_date, Heure_debut, Heure_fin, StatutID) values (uuid_generate_v4(),'Okto','Centre de foire','image2','description2','casj1001', '29-JUN-19', '11:00:00', '12:00:00',1);
EOSQL
