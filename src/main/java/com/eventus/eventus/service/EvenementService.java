package com.eventus.eventus.service;

import com.eventus.eventus.dao.EvenementDao;
import com.eventus.eventus.model.Evenement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class EvenementService {

    private final EvenementDao evenementDao;

    @Autowired
    public EvenementService(@Qualifier("postgres") EvenementDao evenementDao) {
        this.evenementDao = evenementDao;
    }

    public int addEvenement(Evenement evenement) {
        return evenementDao.insertEvenement(evenement);
    }

    public List<Evenement> getAllEvenements() {
        return evenementDao.selectAllEvenements();
    }

    public Optional<Evenement> getEvenementById(UUID id) {
       return evenementDao.selectEvenementById(id);
    }

    public int deleteEvenement(UUID id) {
        return evenementDao.deleteEvenementById(id);
    }

    public int updateEvenement(UUID id, Evenement newEvenement){
        return evenementDao.updateEvenementById(id, newEvenement);
    }
}
