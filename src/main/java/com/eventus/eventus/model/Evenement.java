package com.eventus.eventus.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

public class Evenement {

    private final UUID id;

    private final String nom;
    private final String lieu;
    private final String imageURL;
    private final String description;
    private final String cip;
    private final LocalDate date;
    private final LocalTime heureDebut;
    private final LocalTime heureFin;
    private final int statutId;

    public Evenement(UUID id, String nom, String lieu, String imageURL, String description, String cip, LocalDate date,  LocalTime heureDebut, LocalTime heureFin, int statutId) {
        this.id = id;
        this.nom = nom;
        this.lieu = lieu;
        this.imageURL = imageURL;
        this.description = description;
        this.cip = cip;
        this.date = date;
        this.heureDebut = heureDebut;
        this.heureFin = heureFin;
        this.statutId = statutId;
    }

    public UUID getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getLieu() {
        return lieu;
    }

    public LocalDate getDate() {
        return date;
    }

    public LocalTime getHeureDebut() {
        return heureDebut;
    }

    public LocalTime getHeureFin() {
        return heureFin;
    }


    public String getImageURL() {
        return imageURL;
    }

    public String getDescription() {
        return description;
    }

    public String getCip() {
        return cip;
    }

    public int getStatutId() {
        return statutId;
    }
}
