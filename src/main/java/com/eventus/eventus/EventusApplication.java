package com.eventus.eventus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;

@SpringBootApplication
@Controller
public class EventusApplication {

    public static void main(String[] args) {
        SpringApplication.run(com.eventus.eventus.EventusApplication.class, args);
    }

}
