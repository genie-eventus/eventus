package com.eventus.eventus.dao;

import com.eventus.eventus.model.Evenement;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface EvenementDao {

    int insertEvenement(UUID id, Evenement evenement);

    default int insertEvenement(Evenement evenement) {
        UUID id = UUID.randomUUID();
        return insertEvenement(id, evenement);
    }

    List<Evenement> selectAllEvenements();

    Optional<Evenement> selectEvenementById(UUID id);

    int deleteEvenementById(UUID id);

    int updateEvenementById(UUID id, Evenement evenement);
}
