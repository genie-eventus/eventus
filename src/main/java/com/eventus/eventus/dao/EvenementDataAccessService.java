package com.eventus.eventus.dao;

import com.eventus.eventus.model.Evenement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("postgres")
public class EvenementDataAccessService implements EvenementDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public EvenementDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertEvenement(UUID id, Evenement evenement) {

        String sql = "" +
                "INSERT INTO evenement (" +
                " EventID, " +
                " Event_nom, " +
                " Event_lieu, " +
                " Event_image, " +
                " Event_description, " +
                " CIP, " +
                " Event_date, " +
                " Heure_debut, " +
                " Heure_fin, " +
                " StatutID) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        return jdbcTemplate.update(
                sql,
                id,
                evenement.getNom(),
                evenement.getLieu(),
                evenement.getImageURL(),
                evenement.getDescription(),
                evenement.getCip(),
                evenement.getDate(),
                evenement.getHeureDebut(),
                evenement.getHeureFin(),
                evenement.getStatutId());
    }

    @Override
    public List<Evenement> selectAllEvenements() {
        final String sql = "SELECT EventID, Event_nom, Event_lieu, Event_image, Event_description, CIP, Event_date, Heure_debut, Heure_fin, StatutID FROM evenement";
        List<Evenement> evenements = jdbcTemplate.query(sql, (resultSet, i) -> {
            UUID id = UUID.fromString(resultSet.getString("EventID"));
            String name = resultSet.getString("Event_nom");
            String lieu = resultSet.getString("Event_lieu");
            String imageURL = resultSet.getString("Event_image");
            String description = resultSet.getString("Event_description");
            String cip = resultSet.getString("CIP");
            LocalDate date = (resultSet.getDate("Event_date")).toLocalDate();
            LocalTime heureDebut = (resultSet.getTime("Heure_debut")).toLocalTime();
            LocalTime heureFin = (resultSet.getTime("Heure_fin")).toLocalTime();
            int statutId = resultSet.getInt("StatutID");

            return new Evenement(id, name, lieu, imageURL, description, cip, date, heureDebut, heureFin, statutId);
        });
        return evenements;
    }

    @Override
    public Optional<Evenement> selectEvenementById(UUID id) {
        final String sql = "SELECT EventID, Event_nom, Event_lieu, Event_image, Event_description, CIP, Event_date, Heure_debut, Heure_fin, StatutID FROM evenement WHERE EventID = ?";
        Evenement evenement = jdbcTemplate.queryForObject(sql, new Object[]{id}, (resultSet, i) -> {
            UUID eventID = UUID.fromString(resultSet.getString("EventID"));
            String name = resultSet.getString("Event_nom");
            String lieu = resultSet.getString("Event_lieu");
            String imageURL = resultSet.getString("Event_image");
            String description = resultSet.getString("Event_description");
            String cip = resultSet.getString("CIP");
            LocalDate date = (resultSet.getDate("Event_date")).toLocalDate();
            LocalTime heureDebut = (resultSet.getTime("Heure_debut")).toLocalTime();
            LocalTime heureFin = (resultSet.getTime("Heure_fin")).toLocalTime();
            int statutId = resultSet.getInt("StatutID");

            return new Evenement(eventID, name, lieu, imageURL, description, cip, date, heureDebut, heureFin, statutId);
        });
        return Optional.ofNullable(evenement);
    }

    @Override
    public int deleteEvenementById(UUID id) {
        String sql = "" +
                "DELETE FROM evenement " +
                "WHERE EventID = ?";
        return jdbcTemplate.update(sql, id);
    }

    @Override
    public int updateEvenementById(UUID id, Evenement evenement) {

        String sql = "" +
                "UPDATE evenement" +
                " SET"+
                " Event_nom = ?, " +
                " Event_lieu = ?, " +
                " Event_image = ?, " +
                " Event_description = ?, " +
                " CIP = ?, " +
                " Event_date = ?, " +
                " Heure_debut = ?, " +
                " Heure_fin = ?, " +
                " StatutID = ? " +
                "WHERE EventID = ?";

        return jdbcTemplate.update(
                sql,
                evenement.getNom(),
                evenement.getLieu(),
                evenement.getImageURL(),
                evenement.getDescription(),
                evenement.getCip(),
                evenement.getDate(),
                evenement.getHeureDebut(),
                evenement.getHeureFin(),
                evenement.getStatutId(),
                id);
    }
}
