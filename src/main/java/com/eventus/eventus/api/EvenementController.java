package com.eventus.eventus.api;

import com.eventus.eventus.exception.ResourceNotFoundException;
import com.eventus.eventus.model.Evenement;
import com.eventus.eventus.service.EvenementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("api/v1/evenement")
@RestController
public class EvenementController {

    private final EvenementService evenementService;

    @Autowired
    public EvenementController(EvenementService evenementService) {
        this.evenementService = evenementService;
    }

    @PostMapping
    public void addEvenement(@Valid @NonNull @RequestBody Evenement evenement) {
        evenementService.addEvenement(evenement);
    }

    @CrossOrigin
    @GetMapping
    public List<Evenement> getAllEvenements() {
        return evenementService.getAllEvenements();
    }

    @GetMapping(path = "{id}")
    public Evenement getEvenementById(@PathVariable("id") UUID id) {
        return evenementService.getEvenementById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Evenement doesn't exist with id :" + id));
    }

    @DeleteMapping(path = "{id}")
    public void deletePersonByID(@PathVariable("id") UUID id) {
        evenementService.deleteEvenement(id);
    }

    @PutMapping(path = "{id}")
    public void updateEvenement(@PathVariable("id") UUID id, @Valid @NonNull @RequestBody Evenement evenementToUpdate) {
        evenementService.updateEvenement(id, evenementToUpdate);
    }
}
