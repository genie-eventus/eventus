CREATE EXTENSION "uuid-ossp";

CREATE TABLE Role
(
  RoleID SERIAL,
  Role_nom VARCHAR(50),
  PRIMARY KEY (RoleID)
);

CREATE TABLE Type
(
  TypeID SERIAL,
  Type_nom VARCHAR(50),
  PRIMARY KEY (TypeID)
);

CREATE TABLE Faculte
(
  FaculteID SERIAL,
  Faculte_nom VARCHAR(50),
  PRIMARY KEY (FaculteID)
);

CREATE TABLE Statut
(
  StatutID SERIAL,
  Statut_nom VARCHAR(50),
  PRIMARY KEY (StatutID)
);

CREATE TABLE Programme
(
  ProgrammeID SERIAL,
  Programme_nom VARCHAR(50),
  FaculteID INT,
  PRIMARY KEY (ProgrammeID),
  FOREIGN KEY (FaculteID) REFERENCES Faculte(FaculteID)
);

CREATE TABLE Usager
(
  CIP CHAR(8) NOT NULL,
  Nom VARCHAR(50),
  Prenom VARCHAR(50),
  Courriel VARCHAR(100),
  Pseudo VARCHAR(50),
  Nombre_ami INT,
  image VARCHAR(100),
  RoleID INT,
  ProgrammeID INT,
  PRIMARY KEY (CIP),
  FOREIGN KEY (RoleID) REFERENCES Role(RoleID),
  FOREIGN KEY (ProgrammeID) REFERENCES Programme(ProgrammeID)
);

CREATE TABLE Evenement
(
  EventID UUID NOT NULL,
  Event_nom VARCHAR(50),
  Heure_debut TIME,
  Heure_fin TIME,
  Event_date DATE,
  Event_lieu VARCHAR(250),
  Event_image VARCHAR(250),
  Event_description VARCHAR(500),
  StatutID INT,
  CIP CHAR(8),
  PRIMARY KEY (EventID),
  FOREIGN KEY (StatutID) REFERENCES Statut(StatutID),
  FOREIGN KEY (CIP) REFERENCES Usager(CIP)
);

CREATE TABLE Amis
(
  cip_ami CHAR(8),
  ami_id SERIAL,
  CIP CHAR(8),
  PRIMARY KEY (ami_id),
  FOREIGN KEY (CIP) REFERENCES Usager(CIP)
);

CREATE TABLE Commentaire
(
  Commentaire_id SERIAL,
  le_commentaire VARCHAR(500),
  CIP CHAR(8),
  EventID UUID,
  PRIMARY KEY (Commentaire_id),
  FOREIGN KEY (CIP) REFERENCES Usager(CIP),
  FOREIGN KEY (EventID) REFERENCES Evenement(EventID)
);

CREATE TABLE Reponse
(
  reponse_id SERIAL,
  la_reponse VARCHAR(500),
  Commentaire_id INT,
  PRIMARY KEY (reponse_id),
  FOREIGN KEY (Commentaire_id) REFERENCES Commentaire(Commentaire_id)
);

CREATE TABLE Participe
(
  participe_id SERIAL,
  Notification INT,
  CIP CHAR(8),
  EventID UUID,
  PRIMARY KEY (participe_id),
  FOREIGN KEY (CIP) REFERENCES Usager(CIP),
  FOREIGN KEY (EventID) REFERENCES Evenement(EventID)
);

CREATE TABLE est_de_type
(
  EventID UUID,
  TypeID INT,
  PRIMARY KEY (EventID, TypeID),
  FOREIGN KEY (EventID) REFERENCES Evenement(EventID),
  FOREIGN KEY (TypeID) REFERENCES Type(TypeID)
);
