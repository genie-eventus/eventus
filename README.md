# eventUS

## Getting started

There is 3 component to run:

- database
- application server (backend)
- developpement frontend server


### Database

- Run only DB:
  `docker-compose up db`
  
- Clean db :
    `docker-compose down --volumes`

### Application server (backend)

- Run only backend :
  `mvnw.cmd spring-boot:run`

- Test view :
  Visit
  http://localhost:8080/api/v1/evenement
  and you should see a json file

- Clean application :
  `mvn clean`

### Frontend

- Run only frontend :
  `docker-compose up frontend`
  or
  `cd frontend && npm install && npm start`

- If you added a dependency: `docker-compose up --build frontend`

- Test view :
  Visit
  http://localhost:3000
  and you should see the frontend

